import pandas as pd
from datetime import datetime, timedelta
import requests
import json
import seaborn as sns
import matplotlib.pyplot as plt
import json
import urllib.request
import folium
import branca.colormap as cm



class DataFormater:

    def __init__(self):

        self.yesterday = datetime.strftime(datetime.now() - timedelta(1), '%m-%d-%Y')
        self.url = f'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/{self.yesterday}.csv'
        self.df = pd.read_csv(self.url, error_bad_lines=False)
        pd.set_option('display.max_rows', 298)
        pd.set_option('display.max_colwidth', 400)
        pd.options.display.float_format = '{:20,.2f}'.format
    
    def Population(self):
        
        url = "https://restcountries.eu/rest/v2/all"
        response = requests.request("GET", url)
        country = []
        population = []
        test = response.json()
        
        for val in test:
            country.append(val['name'])
            population.append(int(val['population']))
        popDict = dict(zip(country,population))
            
        return popDict

    def Data(self):
        
        fuckedNamed = {
            'Bahamas, The' : 'Bahamas', 'Bolivia': 'Bolivia (Plurinational State of)', 'Brunei' : 'Brunei Darussalam',
            'Congo (Brazzaville)' : 'Congo', 'Congo (Kinshasa)' : 'Congo (Democratic Republic of the)', "Cote d'Ivoire" : "Côte d'Ivoire",
            'Czechia' : 'Czech Republic', 'Eswatini' : 'Swaziland', 'Gambia, The' : 'Gambia', 'Iran' : 'Iran (Islamic Republic of)', 
            'Korea, South' : 'Korea (Republic of)', 'Kosovo' : 'Republic of Kosovo', 'Moldova' : 'Moldova (Republic of)', 'North Macedonia' : 'Macedonia (the former Yugoslav Republic of)',
            'Reunion' : 'Réunion', 'Russia' : 'Russian Federation', 'Tanzania' : 'Tanzania, United Republic of', 'The Bahamas' : 'Bahamas', 'The Gambia' : 'Gambia', 'US' : 'United States of America',
            'United Kingdom' : 'United Kingdom of Great Britain and Northern Ireland', 'Venezuela' : 'Venezuela (Bolivarian Republic of)', 'Vietnam' : 'Viet Nam', 'Taiwan*' : 'Taiwan',
            }

        for item in fuckedNamed:

            self.df = self.df.replace(item, fuckedNamed[item])

        self.popcount = DataFormater().Population()
        self.df = self.df.drop([134, 39, 29, 47])
        self.df['Population'] = self.df['Country_Region'].map(self.popcount)

        return self.df

class BarGraph:

    def __init__(self):

        self.df = DataFormater().Data()
        
        self.df = self.df.groupby('Country_Region')[['Confirmed','Deaths','Recovered']].sum().reset_index()
        self.data = self.df
        self.possibleValue = ['Confirmed', 'Deaths', 'Recovered']

    def BarTotalTop20(self, sortby):

        if sortby in self.possibleValue:

            self.data = self.data.sort_values(by=[sortby], ascending=False).head(20).reset_index(drop=True)
            ax = sns.barplot(x=self.data[sortby], y=self.data['Country_Region'])
            ax.set(ylabel = 'Countries')
            plt.title('Covid-19 cases by Country')
            plt.savefig(f'Total-{sortby}.png')
        
        else:
            print('Invalid Value')

    def BarPerCapitaTop20(self, sortby):

        if sortby in self.possibleValue:

            self.data[sortby] = self.data[sortby]/self.data['Population']
            self.data = self.data.sort_values(by=[sortby], ascending=False).head(20).reset_index(drop=True)
            ax = sns.barplot(x=self.data[sortby], y=self.data['Country_Region'])
            ax.set(xlabel = f'{sortby} Per Capita', ylabel = 'Countries')
            plt.title('Covid-19 cases by Country')
            plt.savefig(f'Capita-{sortby}.png')
        
        else:
            print('Invalid Value')

class choroplethMap:

    def __init__(self):

        self.df = DataFormater().Data()
        self.geojsonurl = "https://raw.githubusercontent.com/plotly/datasets/master/geojson-counties-fips.json"

        self.usGeo = self.geojsonurl
        self.df = self.df.loc[self.df['Country_Region'].isin(['US', 'United States of America'])]
        self.df.dropna(inplace=True)
        self.df['FIPS'] = self.df['FIPS'].astype(int)
   
        with urllib.request.urlopen(self.geojsonurl) as url:
            self.requestedjson = json.loads(url.read().decode())
        

        

    def graph(self, sortby):
        
        self.df = self.df.set_index('FIPS')
        self.df = self.df['Deaths']
        fips = self.df.index.tolist()
        
        for item in self.requestedjson['features']:

            if int(item['id']) not in fips == True:
                self.requestedjson['features'].remove(item)

        self.linear = cm.LinearColormap(
            ['green', 'yellow', 'red'],
            vmin=0, vmax=10000
        )
        self.m = folium.Map(location=[37, -102], zoom_start=5)

        folium.GeoJson(
            self.requestedjson,
            style_function=lambda feature: {
        'fillColor': self.linear(self.df.loc[int(feature['id'])]),
        'color': 'black',
        'weight': 0,
        'dashArray': '5, 5'
    }
        ).add_to(self.m)
        
        folium.LayerControl().add_to(self.m)
        self.m.save('#292_folium_chloropleth_USA1.html')

    
if __name__ == '__main__':
    choroplethMap().graph('Confirmed')
